package io.lewandowski.workshop.collections;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class PA06DateTime {

    static String instantToUTC(Instant instant){
        return instant.atZone(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_DATE_TIME);
    }

    static String instantToEuropeWarsaw(Instant instant){
        return instant.atZone(ZoneId.of("Europe/Warsaw")).format(DateTimeFormatter.ISO_DATE_TIME);
    }

    static LocalDateTime localDateForUTC(){
        Clock fixedClock = Clock.fixed(Instant.ofEpochSecond(0), ZoneId.of("UTC"));
        return LocalDateTime.now(fixedClock);
    }

    static LocalDateTime addAndSubtractOneMonth(LocalDateTime input){
        return input.plus(Duration.ofDays(30)).minus(Duration.ofDays(30));
    }
}
