package io.lewandowski.workshop.architecture.solid.ocp;

public class PY02PriceCalculator {

    public double calculateFee(Offer offer) {
        return offer.calculatePrice();
    }
}

abstract class Offer {
    int price;

    abstract String formatPrice();

    abstract double calculatePrice();
}

class NormalBuy extends Offer {


    @Override
    String formatPrice() {
        return "Cena: " + price;
    }

    @Override
    double calculatePrice() {
        return price * 0.1;
    }
}

class InstallmentPay extends Offer {

    @Override
    String formatPrice() {
        return "Cena: " + price / 10 + " * 10 rat";
    }

    @Override
    double calculatePrice() {
        return price * 0.11;
    }
}
