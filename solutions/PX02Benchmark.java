package io.lewandowski.workshop.architecture;


import io.lewandowski.workshop.architecture.performance.MyTask;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;

@State(Scope.Benchmark)
public class PX02Benchmark {

    public final double PI = Math.PI;

    @Benchmark()
    public void helloWorld(Blackhole blackhole) {
        Double magicRandom = PI * 100_000;
        ArrayList<Integer> business = new MyTask().business(magicRandom.intValue());
        blackhole.consume(blackhole);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(PX02Benchmark.class.getSimpleName()).forks(10).warmupIterations(10).build();

        new Runner(options).run();
    }
}
