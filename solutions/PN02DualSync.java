package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class PN02DualSync {

    private final Object syncA = new Object();
    private final Object syncB = new Object();

    public String businessA() throws InterruptedException {
        synchronized (syncA) {
            log.info("Business A started");
            TimeUnit.SECONDS.sleep(1);
            log.info("Business A will end soon");
        }
        return "A";
    }

    public String businessB() throws InterruptedException {
        synchronized (syncB) {
            log.info("Business A started");
            TimeUnit.SECONDS.sleep(1);
            log.info("Business A will end soon");
        }
        return "B";
    }
}
