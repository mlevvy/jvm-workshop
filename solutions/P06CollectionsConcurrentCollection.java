package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
public class P06CollectionsConcurrentCollection {

    private final Queue<Integer> theList = new ConcurrentLinkedQueue<>();

    private final Random randomGenerator = new Random();

    public void addRandom() {
        int i = randomGenerator.nextInt(10_000);
        log.info("Adding {}", i);
        theList.add(i);
    }

    public void showMeException() {
        for (Integer i : theList) {
            log.info("Element is: {}", i);
        }
    }


}
