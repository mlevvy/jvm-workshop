package io.lewandowski.workshop.architecture.eventsourcing.repository;

import io.lewandowski.workshop.architecture.eventsourcing.domain.AggregateId;
import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import io.lewandowski.workshop.architecture.eventsourcing.models.ReadModel;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Data
public class ModelsRepository<T extends ReadModel> {

    private Integer currentPosition = 0;

    private Map<AggregateId, T> uniqueKeysMap = new HashMap<>();

    private Supplier<T> supplier;

    public ModelsRepository(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public T get(AggregateId aggregateId) {
        return uniqueKeysMap.computeIfAbsent(aggregateId, it -> supplier.get());
    }

    public void update(EventSourceRepository eventSourceRepository) {
        if(currentPosition < eventSourceRepository.getPosition()){
            update(eventSourceRepository.getEventsSince(currentPosition));
        }

    }

    private void update(List<KeyPressed> events) {
        events.forEach(event ->
                    get(event.getAggregateId()).update(event)
        );
        currentPosition += events.size();
    }
}
