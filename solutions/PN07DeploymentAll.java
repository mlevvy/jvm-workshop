package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class PN07DeploymentAll {
    private boolean projectBuild = false;

    public synchronized void buildProject() throws InterruptedException {
        log.info("Building project");
        projectBuild = true;
        TimeUnit.MILLISECONDS.sleep(100);
        log.info("Project built");
        notifyAll();
    }

    public synchronized String deployProject() throws InterruptedException {
        while (!projectBuild) {
            log.info("Waiting for project build");
            wait();
            log.info("Unlocked. Deploy project now!");
        }
        return "Deployed";
    }
}
