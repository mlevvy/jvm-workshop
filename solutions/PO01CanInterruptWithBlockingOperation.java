package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
class PO01CanInterruptWithBlockingOperation extends Thread {

    @Override
    public void run() {
        while (true) {
            log.info("Working...");

            try {
                TimeUnit.MINUTES.sleep(1);
            } catch (InterruptedException e) {
                log.info("Am I interrupted? {}", this.isInterrupted());
                interrupt();
            }

            if (Thread.currentThread().isInterrupted()) {
                log.info("Oh my good. I got an interruption!");
                break;
            }
        }
        log.info("Done");
    }

}
