package io.lewandowski.workshop.collections;

public class PA01Cpu implements Comparable<PA01Cpu> {

    private final int cores;
    private final int threads;
    private final int cache;

    public PA01Cpu(int cores, int threads, int cache) {
        this.cores = cores;
        this.threads = threads;
        this.cache = cache;
    }

    @Override
    public String toString() {
        return "Cpu{" +
                "cores=" + cores +
                ", threads=" + threads +
                ", cache=" + cache +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PA01Cpu pa01Cpu = (PA01Cpu) o;

        if (cores != pa01Cpu.cores) return false;
        if (threads != pa01Cpu.threads) return false;
        return cache == pa01Cpu.cache;
    }

    @Override
    public int hashCode() {
        int result = cores;
        result = 31 * result + threads;
        result = 31 * result + cache;
        return result;
    }

    @Override
    public int compareTo(PA01Cpu that) {
        if (this.cores < that.cores) {
            return -1;
        } else if (this.cores > that.cores) {
            return 1;
        }

        if (this.threads < that.threads) {
            return -1;
        } else if (this.threads > that.threads) {
            return 1;
        }

        if (this.cache < that.cache) {
            return -1;
        } else if (this.cache > that.cache) {
            return 1;
        }
        return 0;
    }

}