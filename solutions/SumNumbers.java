package io.lewandowski.workshop.architecture.eventsourcing.models;

import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import lombok.Data;

@Data
public class SumNumbers implements ReadModel{
    private Integer sumNumbers = 0;

    public void update(KeyPressed event) {
        char value = event.getValue();
        if (Character.isDigit(value)){
            sumNumbers += Character.getNumericValue(value);
        }
    }

    @Override
    public ReadModel construct() {
        return new SumNumbers();
    }
}
