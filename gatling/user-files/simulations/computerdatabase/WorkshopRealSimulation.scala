package computerdatabase

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class WorkshopRealSimulation extends Simulation {

  val httpConf = http
    .baseURLs("http://localhost:8080")

  val scn = scenario("Do sth")
    .exec(http("Do sth")
      .get("/real/8458741")
      .check(status.is(200), jsonPath("$.result[5]").is("8458741"))
    )

  setUp(scn.inject(
    //Modify here
    atOnceUsers(1)

  ).protocols(httpConf))

}
