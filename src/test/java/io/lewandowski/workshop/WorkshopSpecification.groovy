package io.lewandowski.workshop

import com.google.common.util.concurrent.ThreadFactoryBuilder
import io.lewandowski.workshop.architecture.PW01CompletableFuture
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class WorkshopSpecification extends Specification {

    ExecutorService executor = Executors.newFixedThreadPool(20, new ThreadFactoryBuilder().setNameFormat("WorkshopExecutor-%d").build())

    Logger log = LoggerFactory.getLogger(WorkshopSpecification.class)

    PW01CompletableFuture service = new PW01CompletableFuture()


}
