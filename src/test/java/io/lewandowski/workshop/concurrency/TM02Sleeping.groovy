package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.TimeUnit

class TM02Sleeping extends WorkshopSpecification {

    def "Classic sleep"() {
        expect:
            Thread.sleep(1_000)
    }

    def "Better sleep"() {
        expect:
            log.info("Start")
            TimeUnit.NANOSECONDS.sleep(10)
            log.info("Slept 10 nanoseconds")
            TimeUnit.MICROSECONDS.sleep(10)
            log.info("Slept 10 microseconds")
            TimeUnit.MILLISECONDS.sleep(10)
            log.info("Slept 10 milliseconds")
    }

    /**
     * Yield mówi do schedulera: "W sumie, to mogę teraz poczekać, jak masz jakieś ważniejsze wątki, to możesz je wykonywać"
     */
    def "Yield allows other threads to do their job"() {
        expect:
            Thread.yield()
    }

    /**
     * Next: TN00NotShared
     */

}
