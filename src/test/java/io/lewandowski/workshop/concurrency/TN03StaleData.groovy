package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.Callable
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class TN03StaleData extends WorkshopSpecification {

    public static final String OK = "OK"

    def "Synchronizing only on write is not a good idea"() {
        given:
            PN03MutableStringSingleSynchronized sut = new PN03MutableStringSingleSynchronized()
        when:
            Future getFuture = executor.submit({ it -> sut.notNullGet() } as Callable)
            executor.submit({ it -> sut.setMyMagicString(OK) } as Thread).get()
            getFuture.get(1, TimeUnit.SECONDS) == OK
        then:
            thrown(TimeoutException) //Should not be thrown
    }

    /**
     * Ćwiczenie. Napraw test z odpowiednim użyciem synchronizacji
     *
     * Hint: Każda zmienna którą synchronizujesz powinna być synchronizowana na odczycie jak i na zapisie z użyciema tego samego lock'a.
     */
    def "Synchronizing on read and write is the correct way"() {
        given:
            PN03MutableStringDoubleSynchronized sut = new PN03MutableStringDoubleSynchronized()
        when:
            Future getFuture = executor.submit({ it -> sut.notNullGet() } as Callable)
            executor.submit({ it -> sut.setMyMagicString(OK) } as Thread).get()
        then:
            getFuture.get(1, TimeUnit.SECONDS) == OK
    }

    /**
     * Ćwiczenie. Napraw test z odpowiednim zmiennej typu volatile
     *
     * Hint: Oznaczając zmienną jako volatile, informujesz środowisko uruchomieniowe o nie cachowaniu tej zmiennej.
     *
     * Uwaga: Zmienna volatile nie rozwiązuje problemu read-and-modify. Zmienne volatile można używać do komunikacji gdzie jeden wątek zapisuje a wiele czyta.
     */
    def "Exposing visibility using volatile"() {
        given:
            PN03MutableStringVolatileSynchronized sut = new PN03MutableStringVolatileSynchronized()
        when:
            Future getFuture = executor.submit({ it -> sut.notNullGet() } as Callable)
            executor.submit({ it -> sut.setMyMagicString(OK) } as Thread).get()
        then:
            getFuture.get(1, TimeUnit.SECONDS) == OK
    }

    /**
     * Next: TN04ThreadLocal
     */

}
