package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.*

class TN07Signaling extends WorkshopSpecification {

    def "How to use it - just normal?"() {
        when:
            wait()
        then:
            thrown(IllegalMonitorStateException)
    }

    /** Note that:
     * Możesz usunąć wait i czekać w pętli, ale wtedy procesor będzie cały czas pracował
     * W pętli musimy sprawdzać wyjątek. Co jeżeli zostaniemy obudzeni a nie będzie pracy?
     * Dostęp do zmiennej z warunkiem powinien być synchronizowany.
     * */

    def "How to use it - correct rule"() {
        given:
            PN07Deployment sut = new PN07Deployment()
            Future<String> waitingTask = executor.submit({ -> sut.deployProject() } as Callable<String>)
        when:
            waitingTask.get(1, TimeUnit.SECONDS)
        then:
            thrown(TimeoutException)
    }

    def "How notify works"() {
        given:
            PN07Deployment sut = new PN07Deployment()
        when:
            Future<String> waitingTask = executor.submit({ -> sut.deployProject() } as Callable<String>)
            TimeUnit.MILLISECONDS.sleep(200)
            executor.submit({ -> sut.buildProject() } as Runnable)
        then:
            waitingTask.get(1, TimeUnit.SECONDS) == "Deployed"
    }

    /**
     * Ćwiczenie. Powiadom wszystkie czekające wątki.
     * @return
     */
    def "How notifyAll works"() {
        given:
            PN07DeploymentAll sut = new PN07DeploymentAll()
        when:
            Future<String> deploy1 = executor.submit({ -> sut.deployProject() } as Callable<String>)
            Future<String> deploy2 = executor.submit({ -> sut.deployProject() } as Callable<String>)
            TimeUnit.MILLISECONDS.sleep(200)
            executor.submit({ -> sut.buildProject() } as Runnable)
        then:
            deploy1.get(1, TimeUnit.SECONDS) == "Deployed"
            deploy2.get(1, TimeUnit.SECONDS) == "Deployed"
    }

    def "How CountDownLatch Works"() {
        given:
            CountDownLatch latch = new CountDownLatch(2)
            PN07CountDownLatch sut = new PN07CountDownLatch(latch)
        when:
            executor.submit({ -> sut.buildProject() } as Runnable)
            executor.submit({ -> sut.buildProject() } as Runnable)
            Future<String> deploy = executor.submit({ -> sut.deployProject() } as Callable<String>)
        then:
            deploy.get(1, TimeUnit.SECONDS) == "Deployed"
    }

    /**
     * GOTO Slides (Zatrzymywanie aplikacji/wątku)
     */

}
