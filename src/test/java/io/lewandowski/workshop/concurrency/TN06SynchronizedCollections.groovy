package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

class TN06SynchronizedCollections extends WorkshopSpecification {

    def "See the difference"() {
        expect:
            Map synchronizedMap = Collections.synchronizedMap([:])
            log.info("Concurrent Map: {}", synchronizedMap.getClass())

            Map concurrentHashMap = new ConcurrentHashMap([:])
            log.info("Concurrent Map: {}", concurrentHashMap.getClass())
    }

    def "We have to use synchronized collections - without locking doesn't work"() {
        given:
            P06Collections collections = new P06Collections()
        when:
            (1..10_000).collect { i -> executor.submit({ -> collections.addRandom() }) }
            collections.showMeException()
        then:
            TimeUnit.SECONDS.sleep(1)
            thrown(ConcurrentModificationException)
    }

    def "We have to use synchronized collections - synchronized collections also doesn't work"() {
        given:
            P06CollectionsWithSynchronizedCollections collections = new P06CollectionsWithSynchronizedCollections()
        when:
            (1..10_000).collect { i -> executor.submit({ -> collections.addRandom() }) }
            collections.showMeException()
        then:
            TimeUnit.SECONDS.sleep(1)
            thrown(ConcurrentModificationException)
    }

    /**
     * Należy zwrócić uwagę, na jakim obiekcie synchoronizujemy się
     */
    def "We have to use synchronized collections - synchronize external client doesn't work"() {
        given:
            P06CollectionsWithSynchronizedCollectionsAndExternalLockWrong collections = new P06CollectionsWithSynchronizedCollectionsAndExternalLockWrong()
        when:
            (1..10_000).collect { i -> executor.submit({ -> collections.addRandom() }) }
            collections.showMeException()
        then:
            TimeUnit.SECONDS.sleep(1)
            thrown(ConcurrentModificationException)
    }

    /**
     * Ćwiczenie. Napraw testy dodając synchronizację w metodzie showMeException.
     */
    def "Repair using synchronization on correct lock"() {
        given:
            P06CollectionsWithSynchronizedCollectionsAndLocking collections = new P06CollectionsWithSynchronizedCollectionsAndLocking()
        when:
            (1..10_000).collect { i -> executor.submit({ -> collections.addRandom() }) }
            collections.showMeException()
        then:
            TimeUnit.SECONDS.sleep(1)
            notThrown(ConcurrentModificationException)
    }

    /**
     * Ćwiczenie. Napraw testy używając współbierznej kolekcji
     */
    def "Repair using synchronization using synchronized collections"() {
        given:
            P06CollectionsConcurrentCollection collections = new P06CollectionsConcurrentCollection()
        when:
            (1..10_000).collect { i -> executor.submit({ -> collections.addRandom() }) }
            collections.showMeException()
        then:
            TimeUnit.SECONDS.sleep(1)
            notThrown(ConcurrentModificationException)
    }

    /**
     * Next: TN07Signaling
     */

}
