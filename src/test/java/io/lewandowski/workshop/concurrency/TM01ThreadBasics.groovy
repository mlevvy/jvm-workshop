package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class TM01ThreadBasics extends WorkshopSpecification {

    def "This is how to create runnable - but it runs in the same thread"() {
        given:
            log.info("Hello from test thread")
            PM01MyFirstRunnable runnable = new PM01MyFirstRunnable()
        expect:
            runnable.run()
    }

    def "This is how to create runnable - using thread"() {
        given:
            log.info("Hello from test thread")
            PM01MyFirstRunnable runnable = new PM01MyFirstRunnable()
        when:
            Thread aThread = new Thread(runnable)
        then:
            aThread.start()
    }

    def "This is how to create runnable - using executors"() {
        given:
            log.info("Hello from test thread")
            Executor executorService = Executors.newCachedThreadPool()
            PM01MyFirstRunnable runnable = new PM01MyFirstRunnable()
        when:
            executorService.submit(runnable)
        then:
            TimeUnit.MILLISECONDS.sleep(100)
    }

    /**
     * ćwiczenie. Zaimplementuj PM01MyFirstCallable.
     */
    def "This is how to create callable - using executors"() {
        given:
            log.info("Hello from test thread")
            PM01MyFirstCallable callable = new PM01MyFirstCallable()
        when:
            Future<String> submit = executor.submit(callable)
        then:
            submit.get() == "Callable"
    }

    /**
     * Next: TM02Sleeping
     */

}
