package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class TO02StoppingScheduler extends WorkshopSpecification {

    /** Executor API
     * shutdown - nie przyjmuje nowych zadań, nie przerywa istniejących zadań
     * shutdownNow - nie przyjmuje nowych zadań, przerywa istniejące zadania
     * awaitTermination - czeka X czasu, a następnie zwraca status, czy wszystko się zakończyło
     *
     * ćwiczenie dodatkowe:
     * * Doprowadź test do takiego poziomu żeby scheduler wyrzucił RejectedExecutionException
     */

    def "Can interrupt - blocking operation"() {
        given:
            Executor executor = Executors.newSingleThreadExecutor()
            Future myTask = executor.submit({ -> TimeUnit.SECONDS.sleep(2) } as Runnable)
            TimeUnit.MILLISECONDS.sleep(100)
        when:
            executor.shutdownNow()
            boolean isTerminated = executor.awaitTermination(100, TimeUnit.MILLISECONDS)
        then:
            isTerminated
            myTask.done
    }

    /**
     * ćwiczenie dodatkowe:
     * Zakomentuj linię z break i sprawdź co się stanie.
     *
     */
    def "Can interrupt - hard work"() {
        given:
            Executor executor = Executors.newSingleThreadExecutor()
            Future myTask = executor.submit({ ->
                while (true) {
                    if (Thread.currentThread().isInterrupted()) {
                        log.info("Being interrupted")
                        break
                    }
                }
            } as Runnable)
            TimeUnit.MILLISECONDS.sleep(100)
        when:
            executor.shutdownNow()
            boolean isTerminated = executor.awaitTermination(100, TimeUnit.MILLISECONDS)
        then:
            isTerminated
            myTask.done
    }

}
