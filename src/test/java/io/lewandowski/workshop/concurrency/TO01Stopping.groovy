package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.TimeUnit

class TO01Stopping extends WorkshopSpecification {

    def "Can not interrupt - normal thread"() {
        given:
            PO01CanNotInterrupt sut = new PO01CanNotInterrupt()
        when:
            sut.start()
            TimeUnit.MILLISECONDS.sleep(100)
            sut.interrupt()
            TimeUnit.MILLISECONDS.sleep(100)
        then:
            sut.isAlive()
            sut.stop() //Housekeeping

    }

    def "Can interrupt - normal thread"() {
        given:
            PO01CanInterruptThread sut = new PO01CanInterruptThread()
        when:
            sut.start()
            TimeUnit.MILLISECONDS.sleep(100)
            sut.interrupt()
            TimeUnit.MILLISECONDS.sleep(100)
        then:
            !sut.isAlive()
    }

    /**
     * ćwiczenie. Obsłuż prawidłowo przerwanie. Ustaw flagę o przerwaniu wywołując metodę interrupted()
     */
    def "Can interrupt - handling exception"() {
        given:
            PO01CanInterruptWithBlockingOperation sut = new PO01CanInterruptWithBlockingOperation()
        when:
            sut.start()
            TimeUnit.MILLISECONDS.sleep(100)
            sut.interrupt()
            TimeUnit.MILLISECONDS.sleep(100)
        then:
            !sut.isAlive()
    }

}
