package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.TimeUnit

class TP01CustomizedScheduler extends WorkshopSpecification {

    def "customized scheduler name"() {
        given:
            ThreadFactory factory = PP01CustomizedScheduler.getThreadFactory()
        when:
            Thread t = factory.newThread({ -> log.info("Doing some work in thread") })
        and: 'Just to see it'
            t.start()
            TimeUnit.MILLISECONDS.sleep(100)
        then:
            t.getName() == "CustomName"
    }

    /** Ćwiczenie. Zmień metodę setName, tak żeby dodawać numer wątku. */
    def "customized scheduler name with format"() {
        given:
            ThreadFactory factory = PP01CustomizedScheduler.getThreadFactoryWithThreadNumber()
        when:
            Thread t1 = factory.newThread({ -> log.info("Doing some work in thread") })
            Thread t2 = factory.newThread({ -> log.info("Doing some work in thread") })
        then:
            t1.getName() == "CustomName-0"
            t2.getName() == "CustomName-1"
    }

    def "use thread factory in executor"() {
        given:
            ThreadFactory factory = PP01CustomizedScheduler.getThreadFactoryWithThreadNumber()
        when:
            ExecutorService executor = Executors.newFixedThreadPool(10, factory)
            executor.submit({ -> log.info("Doing some work in thread") })
            executor.submit({ -> log.info("Doing some work in thread") })
            executor.submit({ -> log.info("Doing some work in thread") })
            executor.shutdown()
        then:
            executor.awaitTermination(100, TimeUnit.MILLISECONDS)
    }

    def "Change thread name to your business logic"() {
        given:
            ThreadFactory factory = PP01CustomizedScheduler.getThreadFactoryWithThreadNumber()
        when:
            ExecutorService executor = Executors.newFixedThreadPool(10, factory)
            executor.submit({ ->
                Thread.currentThread().setName("Business-ID: 123")
                log.info("Doing some work in thread")
            })
            executor.submit({ ->
                Thread.currentThread().setName("Business-ID: 321")
                log.info("Doing some work in thread")
            })
            executor.shutdown()
        then:
            executor.awaitTermination(100, TimeUnit.MILLISECONDS)
    }

    /**
     * Inne rzeczy do ustawienia:
     * .setDaemon(true)
     * .setPriority(Thread.MIN_PRIORITY)
     */
}
