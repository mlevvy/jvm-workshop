package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

import java.util.concurrent.*

class TN02Synchronization extends WorkshopSpecification {

    /**
     * Wadą tego rozwiązania jest procesowanie sekwencyjne zadań.
     */
    @Unroll("Counter works using synchronized. Size is '#testSize.size()'")
    def "Concurrency work with synchronized"() {
        given:
            PN02Synchronized sut = new PN02Synchronized()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..2_000), (1..3_000)]
    }

    @Unroll("Counter works using synchronized. Size is '#testSize.size()'")
    def "Concurrency work with synchronized but faster"() {
        given:
            PN02SynchronizedFaster sut = new PN02SynchronizedFaster()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..2_000), (1..3_000)]
    }

    /**
     * Ćwiczenie. Napraw test z wykorzystaniem dwóch locków
     */
    def "Should finish withing seconds - two separate operations"() {
        given:
            PN02DualSync dualSync = new PN02DualSync()
            Executor dedicatedExecutor = Executors.newFixedThreadPool(2)
        when:
            dedicatedExecutor.submit({ -> dualSync.businessA() } as Callable<String>)
            Future<String> businessB = dedicatedExecutor.submit({ -> dualSync.businessB() } as Callable<String>)
            businessB.get(1999, TimeUnit.MILLISECONDS)
        then:
            notThrown(TimeoutException)
    }

    def "Should explain Lock Reentrance"() {
        given:
            PN02SyncReEntrace reEntrace = new PN02SyncReEntrace()
        expect:
            executor.submit({ -> reEntrace.outer() })
            Future<String> innerFuture = executor.submit({ -> reEntrace.inner() } as Callable<String>)
        when:
            innerFuture.get(3_000, TimeUnit.MILLISECONDS)
        then:
            thrown(TimeoutException)
    }

    /**
     * GOTO Slides (Klasa Lock)
     */


    @Unroll("Counter works using locks. Size is '#testSize.size()'")
    def "Concurrency work with Lock"() {
        given:
            PN02SynchronizedUsingLock sut = new PN02SynchronizedUsingLock()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..2_000), (1..3_000)]
    }


    @Unroll("Counter works using locks. Size is '#testSize.size()'")
    def "Concurrency work with ReadWriteLock"() {
        given:
            PN02SynchronizedUsingReadWriteLock sut = new PN02SynchronizedUsingReadWriteLock()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..2_000), (1..3_000)]
    }

    /**
     * Next: TN02StaleData
     */


}
