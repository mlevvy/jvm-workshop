package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.Callable

class TN04ThreadLocal extends WorkshopSpecification {

    public static final String THREAD_VALUE = "ThreadValue1"
    public static final String MAIN_THREAD_VALUE = "main"

    /** Hint
     * Ćwiczenie. Zaimplementuj metodę setMyValue
     *
     * Usecase: Trzynanie zmiennych przekazywanych w kontekście na przykład Trace-Id, dla świata mikrousług
     *
     * Hint: Ustaw wartość w zmiennej lokalnej.
     */
    def "Thread Local"() {
        given:
            PN04LocalStorage sut = new PN04LocalStorage()
        when:
            sut.setMyValue(MAIN_THREAD_VALUE)
            String threadValue = executor.submit({ it ->
                sut.setMyValue(THREAD_VALUE)
                sut.getMyThreadLocal()
            } as Callable<String>).get()
        then:
            threadValue == THREAD_VALUE
            sut.getMyThreadLocal() == MAIN_THREAD_VALUE
    }

    /**
     * Next: Slajdy
     */

}
