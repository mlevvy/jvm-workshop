package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

import java.util.concurrent.Callable
import java.util.concurrent.Future

class TN00NotShared extends WorkshopSpecification {

    @Unroll("Thread safe operation on size '#testSize.size()'")
    def "thread safe operation by not sharing state"() {
        given:
            PN00SimpleSafe sut = new PN00SimpleSafe()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.business(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..10_000), (1..100_000)]
    }

    /**
     * Next: TN01Atomicity
     */

}
