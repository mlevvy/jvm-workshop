package io.lewandowski.workshop.concurrency

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

class TN01Atomicity extends WorkshopSpecification {

    @Unroll("Counter sometimes should fail. Size is '#testSize.size()'")
    def "Demonstrates read-and-modify problem."() {
        given:
            PN01NotSafe sut = new PN01NotSafe()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count <= testSize.size() //Should be equal
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..10_000), (1..100_000)]
    }

    @Unroll("Counter works using AtomicInteger. Using AtomicInteger. Size is '#testSize.size()'")
    def "Concurrency work with AtomicInteger"() {
        given:
            PN01CounterSafe sut = new PN01CounterSafe()
            ExecutorService executor = Executors.newCachedThreadPool()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..10_000), (1..100_000)]
    }

    /**
     * Ćwiczenie. Napraw test z użyciem AtomicReference
     *
     * Hint 1: Opakuj finalną klasę Counter w holder AtomicReference
     * Hint 2: AtomicReference wystawia funkcjonalność CAS (CompareAndSwap) pod metodą compareAndSet, która porównuje czy się nie zmieniło, jeżeli nie to zapisuje. Jeżeli się zmieniło, to zwraca false.
     * Hint 3: Skorzystaj z dobrodziejstw Javy8 i skorzystaj z metody: updateAndGet. */

    @Unroll("Counter always correct. Using AtomicReference. Size is '#testSize.size()'")
    def "Concurrency work with AtomicReference"() {
        given:
            PN01CounterWithClass sut = new PN01CounterWithClass()
            ExecutorService executor = Executors.newCachedThreadPool()
        when:
            List<Future<ArrayList<Integer>>> results = testSize.collect { i ->
                executor.submit({ -> sut.doBusiness(345)
                } as Callable<ArrayList<Integer>>)
            }

        then:
            results.forEach { result -> assert result.get() == [1, 3, 5, 15, 23, 69, 115, 345] }
            sut.count == testSize.size()
        where:
            testSize << [(1..10), (1..100), (1..1_000), (1..10_000), (1..100_000)]
    }

    /**
     * Next: TN02Synchronization
     */

}
