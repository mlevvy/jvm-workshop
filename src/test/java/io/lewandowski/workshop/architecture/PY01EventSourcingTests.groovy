package io.lewandowski.workshop.architecture

import io.lewandowski.workshop.architecture.eventsourcing.KeyTracker
import io.lewandowski.workshop.architecture.eventsourcing.repository.EventSourceRepository
import io.lewandowski.workshop.architecture.eventsourcing.repository.ReadModelsRepository
import io.lewandowski.workshop.architecture.eventsourcing.service.CommandService
import io.lewandowski.workshop.architecture.eventsourcing.service.QueryService
import spock.lang.Specification

class PY01EventSourcingTests extends Specification {

    public static final String APPLE = "Apple Keyboard"
    public static final String LOGITECH_KEYBOARD = "Logitech"

    private KeyTracker keyTracker

    def setup() {
        EventSourceRepository eventSourceRepository = new EventSourceRepository()
        ReadModelsRepository readModelsRepository = new ReadModelsRepository()
        CommandService commandService = new CommandService(eventSourceRepository)
        QueryService queryService = new QueryService(eventSourceRepository, readModelsRepository)
        keyTracker = new KeyTracker(commandService, queryService)
    }

    def "should be empty"() {
        expect:
            keyTracker.getNumbersSum(APPLE) == 0
            keyTracker.getUniqueChars(APPLE) == 0
        and:
            keyTracker.getNumbersSum(LOGITECH_KEYBOARD) == 0
            keyTracker.getUniqueChars(LOGITECH_KEYBOARD) == 0
        and:
            keyTracker.getNumbersSum("ufo") == 0
            keyTracker.getUniqueChars("ufo") == 0
    }

    def "should store only for specific aggregateId"() {
        given:
            keyTracker.pressKey(APPLE, 'c' as char)
        expect: 'check for apple keyboard'
            keyTracker.getNumbersSum(APPLE) == 0
            keyTracker.getUniqueChars(APPLE) == 1
        and: 'check for apple keyboard'
            keyTracker.getNumbersSum(LOGITECH_KEYBOARD) == 0
            keyTracker.getUniqueChars(LOGITECH_KEYBOARD) == 0
    }

    def "should store for two different keyboards "() {
        given:
            keyTracker.pressKey(APPLE, 'c' as char)
            keyTracker.pressKey(LOGITECH_KEYBOARD, 'c' as char)
        expect: 'check for apple keyboard'
            keyTracker.getNumbersSum(APPLE) == 0
            keyTracker.getUniqueChars(APPLE) == 1
        and: 'check for apple keyboard'
            keyTracker.getNumbersSum(LOGITECH_KEYBOARD) == 0
            keyTracker.getUniqueChars(LOGITECH_KEYBOARD) == 1
    }

    def "should store numbers"() {
        given:
            keyTracker.pressKey(APPLE, 'c' as char)
            keyTracker.pressKey(APPLE, '1' as char)
            keyTracker.pressKey(APPLE, '2' as char)
            keyTracker.pressKey(APPLE, '1' as char)
        expect:
            keyTracker.getNumbersSum(APPLE) == 4
            keyTracker.getUniqueChars(APPLE) == 3
    }

    def "should not overlap"() {
        when:
            keyTracker.pressKey(APPLE, '1' as char)
        then:
            keyTracker.getNumbersSum(APPLE) == 1
        when:
            keyTracker.pressKey(APPLE, '1' as char)
        then:
            keyTracker.getNumbersSum(APPLE) == 2
    }

    def "random test"() {
        when:
            keyTracker.pressKey(APPLE, '1' as char)
        then:
            keyTracker.getNumbersSum(APPLE) == 1
            keyTracker.getUniqueChars(APPLE) == 1
            keyTracker.getNumbersSum(LOGITECH_KEYBOARD) == 0
            keyTracker.getUniqueChars(LOGITECH_KEYBOARD) == 0
        when:
            keyTracker.pressKey(APPLE, 'a' as char)
            keyTracker.pressKey(LOGITECH_KEYBOARD, '1' as char)
            keyTracker.pressKey(APPLE, '3' as char)
            keyTracker.pressKey(LOGITECH_KEYBOARD, 'z' as char)
        then:
            keyTracker.getNumbersSum(APPLE) == 4
            keyTracker.getUniqueChars(APPLE) == 3
            keyTracker.getNumbersSum(LOGITECH_KEYBOARD) == 1
            keyTracker.getUniqueChars(LOGITECH_KEYBOARD) == 2
    }

}
