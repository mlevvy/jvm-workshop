package io.lewandowski.workshop.architecture

import io.lewandowski.workshop.WorkshopSpecification

import java.util.concurrent.CompletableFuture
import java.util.function.BiFunction

class TW01CompletableFuture extends WorkshopSpecification {

    /** Problem:
     * Kompozycja wielu asynchronicznych zadań.
     * Na przykład dwa zapytania i na obydwa chcę czekać lub czekam tylko na szybsze.
     */

    def "Creating from static"() {
        when:
            CompletableFuture<Integer> a = CompletableFuture.completedFuture(1)
        then:
            a.get() == 1
    }

    /*Hint: Notice the thread*/
    def "Creating from lambda"() {
        when:
            CompletableFuture<String> a = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") })
        then:
            a.get() == "CBA"
    }

    def "Creating from lambda with executor"() {
        when:
            CompletableFuture<String> a = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") }, executor)
        then:
            a.get() == "CBA"
    }

    def "Map operation - using synchronous calls"() {
        when:
            CompletableFuture<String> a = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") }, executor)
        and:
            CompletableFuture<Integer> result = a.thenApply({ input -> input.size() })
        then:
            result.get() == 3
    }

    def "Map operation - using synchronous calls inline"() {
        when:
            CompletableFuture<Boolean> result = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") }, executor)
                    .thenApply({ input -> input.size() })
                    .thenApply({ input -> input % 3 == 0 })
        then:
            result.get()
    }

    def "Map operation - using asynchronous calls"() {
        when:
            CompletableFuture<String> result  = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") }, executor)
                    .thenCompose({ input -> findByNumber(input) })
        then:
            result.get() == "ABC"
    }

    def "Wait for both calls"() {
        when:
            CompletableFuture<String> result  = CompletableFuture
                    .supplyAsync({ -> service.find("ABC") }, executor)
                    .thenCombine(findByNumber("XYZ"), { String a, String b -> "$a, $b" } as BiFunction)
        then:
            result.get() == "CBA, ZYX"
    }

    def "Wait for fastest response"() {
        when:
            String result  = CompletableFuture
                    .supplyAsync({ -> service.find("A") }, executor)
                    .applyToEither(findByNumber("B"), { String b -> b })
                    .get()
        then:
            result == "A" || result == "B"
    }

    private CompletableFuture findByNumber(String input) {
        return CompletableFuture.supplyAsync({
            ->
            service.find(input)},
            executor)

    }

    /**
     * Co więcej?
     * * RxJava
     * * Hystrix
     *
     * GOTO Slides
     */

}
