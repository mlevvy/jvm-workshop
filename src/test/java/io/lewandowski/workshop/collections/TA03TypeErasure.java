package io.lewandowski.workshop.collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TA03TypeErasure {

    @Test(expected = ClassCastException.class) //then
    public void typeErasedProblem() {
        //given
        IntegerList integerList = new IntegerList();

        //when Kompilator nie pozwoli nam wykonac takiej operacji
//        integerList.add("Hello")

        //when rzutujemy na liste, kompilator dodaje metody generyczne, które przyjmują obiekty i wtedy można zrobić sobie problemów
        List stack = integerList;
        stack.add("Hello");
    }
}

/**
 * Go back to slide s
 *
 * */


class IntegerList extends ArrayList<Integer> {

    public boolean add(Integer value) {
        return super.add(value);
    }

    //Type erasue will generate bridge method
    //public boolean add(Object value) {
    //     return super.add((Integer)value);
    //}
}
