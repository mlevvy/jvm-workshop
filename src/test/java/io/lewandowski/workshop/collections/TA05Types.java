package io.lewandowski.workshop.collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TA05Types {

    @Test()
    public void invarianceDemo() {
        //Lista typu B, może być listą obiektów, tylko typu B.
        List<PA05B> bList = new ArrayList<PA05B>();
//        List<PA05B> abList = new ArrayList<PA05A>();
//        List<PA05B> cbList = new ArrayList<PA05C>();
    }

    @Test()
    public void invarianceDemo2() {
        List<PA05A> aList = new ArrayList<PA05A>();
        aList.add(new PA05A());
//        List<PA05B> bList = aList;
        //Gdyby to przypisanie powyżej było by możliwe,
        //tutaj dostałbym ClassCastException w runtime.
//        PA05B aObject = aList.get(0);
    }

    private void invarianceAddImplication(List<PA05B> input){
        input.add(new PA05B()); //Możemy dodać ten typ
        input.add(new PA05C()); //Możemy dodać każdy podtyp B
//        input.add(new PA05A()); //Nie możemy dodać nadtypu B.
    }

    private void invarianceGetImplication(List<PA05B> input){
        //Wiemy, że w tej liście mamy tylko elementy B i tylko B.
        PA05B pa05B = input.get(0);
    }

    //##################################################

    @Test()
    public void objectDemo() {
        //Lista typu obiekt, może być listą obiektów.
        List<Object> objectList = new ArrayList<Object>();
//        List<Object> aNumberList = new ArrayList<Number>();
    }

    private void objectAddImplication(List<Object> input){
        //Możemy wrzucić wszystko
        input.add(new PA05B());
        input.add(new PA05C());
        input.add(new PA05A());
    }

    private void objectGetImplication(List<Object> input){
        //Możemy wrzucić wszystko. Ale jedyne co dostaniemy to Object.
        Object pa05B = input.get(0);
    }


    //##################################################

    @Test()
    public void bivariantDemo() {
        //Lista bivariantna, może zawierać dowolną listę obiektów
        List<?> bivariant1 = new ArrayList<PA05B>();
        List<?> bivariant2 = new ArrayList<PA05A>();
        List<?> bivariant3 = new ArrayList<PA05C>();
    }

    private void bivarianceAddImplication(List<?> input){
        //Nie możemy nic dodać do tej listy, ponieważ nie wiemy jakiego typu ona jest. Może być to lista A lub lista B, lub każdego innego typu.
        //Nie możemy do elementów listy A dodać elementów z listy B.

//        input.add(new PA05B());
//        input.add(new PA05C());
//        input.add(new PA05A());
    }

    private void bivarianceGetImplication(List<?> input){
        //Możemy z tej listy, bezpiecznie konsumować elementy. Wiemy o nich tyle, że będą obiektem.
        Object element = input.get(0);
    }

    //##################################################

    @Test()
    public void covariantDemo() {
        //Lista typu B i niższego, może być listą listę obiektów, typu B i C.
        List<? extends PA05B> bList = new ArrayList<PA05B>();
//        List<? extends PA05B> aList = new ArrayList<PA05A>();
        List<? extends PA05B> cList = new ArrayList<PA05C>();
    }

    private void covariantAddImplication(List<? extends PA05B> input){
        //Nie możemy nic dodać do tej listy, ponieważ nie wiemy jakiego typu ona jest.
        //Faktem jest, że będzie to lista typu B lub C, nie będzie to typ żaden inny, ale tego nie wiemy.
        //W związku z tym, nie możemy nic dodać do tej listy.

//        input.add(new PA05B());
//        input.add(new PA05C());
//        input.add(new PA05A());
    }

    /**
     Producer Extends (the list is the producer, because we can GET elements)
     */
    private void covariantGetImplication(List<? extends PA05B> input){
        //Możemy z tej listy, bezpiecznie konsumować elementy. Wiemy o nich tyle, że będą obiektem typu B.
        //Nie wiemy jaki podtyp dostaniemy.
        PA05B myObjectB = input.get(0);
        PA05A myObjectA = input.get(0);
//        PA05C myObjectC = input.get(0);
    }

    //##################################################

    @Test()
    public void contravariantDemo() {
        //Lista typu B i wyższego, może być listą obiektów, typu B i A.
        List<? super PA05B> bList = new ArrayList<PA05B>();
        List<? super PA05B> aList = new ArrayList<PA05A>();
//        List<? super PA05B> cList = new ArrayList<PA05C>();
    }

    /**
     Consumer Super (the list is the consumer, because we can ADD element to this code)
     */
    private void contravariantAddImplication(List<? super PA05B> input){
        //Wiemy, że możemy dostać listę typu PA05B lub jakiś nadtyp B. Ale jaki? Tego nie wiemy.
        //Dlatego do tej listy, możemy włożyć elementy typu B i jego podtypy.
        input.add(new PA05B());
        input.add(new PA05C());
//        input.add(new PA05A());
    }

    private void contravariantGetImplication(List<? super PA05B> input){
        //Możemy z tej listy, bezpiecznie konsumować elementy. Wiemy o nich tyle, że będą obiektem typu B.
        //Nie wiemy jaki podtyp dostaniemy, więc jedyne co możemy zrobić to jest Object
        Object myObject = input.get(0);
//        PA05B myObjectB = input.get(0);
//        PA05A myObjectA = input.get(0);
//        PA05C myObjectC = input.get(0);
    }

    //##################################################


    /** ćwiczenie myślowe
     * W Collections.addAll, znajduje się meotda która dodaje elementy do listy. Lista jest konsumentem.
     * Dlaczego sygnatura metody jest "Collection<? super T>"?
     *
     * Możesz skopiować i sprawdzić co się stanie.
     *
     * */
    //Dlaczego nie Collection<?>                  ? Nie wiem jaką mam kolekcję, nie mogę do niej dodać.
    //Dlaczego nie Collection<? extends T>        ? Bo możemy dostać jakąś listę typu niższego, niż typ T. Nie mogę pomięszać typów.
    //Dlaczego nie Collection<T> (mimo że działa) ?
    //Działa, ponieważ elementy które dodaje są podtypem T.
    //Jest to mylące, ponieważ pobiebierając wtedy elementy mam gwarancję, że będzie tam typ T.
    //Niestety w takiej kolekcji może być wszystko.
    @Test()
    public void superAPIUseCases() {
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("ABC");

        List<Object> objectList = objects;
        List<? super PA05A> AList = objects;
        List<? super PA05B> BList = objects;
        List<? super PA05C> CList = objects;

        Collections.addAll(objectList, new PA05C());
        Collections.addAll(AList, new PA05C());
        Collections.addAll(BList, new PA05C());
        Collections.addAll(CList, new PA05C());
    }

    @Test()
    public void extendsJavaAPIUseCases() {
        ArrayList<PA05B> objects = new ArrayList<>();
        objects.add(new PA05B());
        objects.addAll(Collections.singletonList(new PA05C()));
        objects.addAll(Collections.singletonList(new PA05B()));
    }

    /** ćwiczenie:
     * 1. Otwórz plik
     * PA05GuessType.java
     *
     * 2. Odkomentuj
     *
     * 3. Uzupełnij nagłówki typów
     * */
























}


//
//class Node<E> {
//    private E data;
//
//    public void setData(E obj) {
//        data = obj;
//    }
//
//    public E getData() {
//        return data;
//}
//}