package io.lewandowski.workshop.collections;

import org.junit.Test;

public class TA02ListVsArray {

    @Test(expected = ArrayStoreException.class) //then
    public void arraysAreRuntime() {
        //given
        Object[] objectArray = new Long[1];
        //when
        objectArray[0] = "PUT STRING :p";
    }

    @Test //then
    public void arraysAreAtCompileTime() {
        //given
        //List<Object> list = new ArrayList<Long>(); //Wont compile

        //when
        //list.add("I don't fit in");
    }

    /**
     * Go back to slide s
     *
     * */
}
