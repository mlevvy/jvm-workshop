package io.lewandowski.workshop.collections;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;

public class TA07Reflection {

    private Logger log = LoggerFactory.getLogger(TA07Reflection.class);

    @Test
    public void withoutReflection() {
        //given:
        PA07Reflection sut = new PA07Reflection();
        //when:
        String result = sut.publicMethod();
        //then:
        assertEquals(result, "PubHello");
    }

    @Test
    public void whatDoIKnowAboutTheClass() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //given
        Class<?> aClass = Class.forName("io.lewandowski.workshop.collections.PA07Reflection");

        //when
        int modifiers = aClass.getModifiers();
        log.info("Modifier.isAbstract(modifiers): {}", Modifier.isAbstract(modifiers));
        log.info("Modifier.isFinal(modifiers): {}", Modifier.isFinal(modifiers));
        log.info("Modifier.isInterface(modifiers): {}", Modifier.isInterface(modifiers));
        log.info("Modifier.isNative(modifiers): {}", Modifier.isNative(modifiers));
        log.info("Modifier.isPrivate(modifiers): {}", Modifier.isPrivate(modifiers));
        log.info("Modifier.isProtected(modifiers): {}", Modifier.isProtected(modifiers));
        log.info("Modifier.isPublic(modifiers): {}", Modifier.isPublic(modifiers));
        log.info("Modifier.isStatic(modifiers): {}", Modifier.isStatic(modifiers));
        log.info("Modifier.isStrict(modifiers): {}", Modifier.isStrict(modifiers));
        log.info("Modifier.isSynchronized(modifiers): {}", Modifier.isSynchronized(modifiers));
        log.info("Modifier.isTransient(modifiers): {}", Modifier.isTransient(modifiers));
        log.info("Modifier.isVolatile(modifiers): {}", Modifier.isVolatile(modifiers));

        Method[] method = aClass.getMethods();
        for (Method m : method) {
            log.info("Method: {}", m);
            Class<?> methodReturnType = m.getReturnType(); //Zawsze będziemy mieli nieznany typ na etapie kąpilacji
        }

    }

    @Test
    public void callPublicMethods() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //given
        Class<?> aClass = Class.forName("io.lewandowski.workshop.collections.PA07Reflection");

        //when
        Method method = aClass.getMethod("publicMethod");

        assertEquals(method.invoke(aClass.newInstance()), "PubHello");
    }

    @Test
    public void callPrivateMethods() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //given
        Class<?> aClass = Class.forName("io.lewandowski.workshop.collections.PA07Reflection");

        //when
        Method method = aClass.getDeclaredMethod("privateMethod");
        method.setAccessible(true); //Potencjalnie SecurityManager może zabronić

        assertEquals(method.invoke(aClass.newInstance()), "PrivHello");
    }

}
