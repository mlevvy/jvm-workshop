package io.lewandowski.workshop.collections;

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneId
import java.time.format.DateTimeFormatter;

class TA06DateTime extends WorkshopSpecification {

    def 'Learn about the clock and instant to calendar conversion'(){
        given:
            Clock fixedClock = Clock.fixed(Instant.ofEpochSecond(0), ZoneId.of("UTC"))
        when:
            Instant now = Instant.now(fixedClock)
            String result = PA06DateTime.instantToUTC(now)
        then:
            result ==  "1970-01-01T00:00:00Z[UTC]"
    }

    /**
     * Ćwiczenie. Zwróć datę w strefie czasowej odpowiedniej dla Polski
     */
    def 'Learn about the clock and instant to Europe/Warsaw Conversion'(){
        given:
            Clock fixedClock = Clock.fixed(Instant.ofEpochSecond(0), ZoneId.of("UTC"))
        when:
            Instant now = Instant.now(fixedClock)
            String result = PA06DateTime.instantToEuropeWarsaw(now)
        then:
            result == "1970-01-01T01:00:00+01:00[Europe/Warsaw]"
    }

    def 'What is LocalDateTime?'(){
        given:
            Clock fixedClock = Clock.fixed(Instant.ofEpochSecond(0), ZoneId.of("Europe/Warsaw"))
        when:
            LocalDateTime localDateTime = LocalDateTime.now(fixedClock)
        then:
            localDateTime.format(DateTimeFormatter.ISO_DATE_TIME) == "1970-01-01T01:00:00"
    }

    /**
     * Ćwiczenie. Test nie będzie przechodził, jeżeli zmienimy strefę czasową. Napraw test tak, żeby przechodził niezależnie od strefy czasowej.
     * Dodaj argument wywołania (VM Options):  -Duser.timezone=Asia/Karachi
     **/
    def 'Learn about local datetime'(){
        when:
            LocalDateTime localDateTime = PA06DateTime.localDateForUTC()
        then:
            localDateTime.format(DateTimeFormatter.ISO_DATE_TIME) == "1970-01-01T00:00:00"
    }

    static final LocalDateTime START_DATE =
            LocalDateTime.of(2017, Month.JANUARY, 1, 0,0)

    /**
     * Ćwiczenie. Dodanie i odjęcie miesiąca nie jest funkcją identycznościową. Wynika to z natury naszego kalendarza.
     * Napraw test tak aby odejmować i dodawać stałą ilość dni, tak żeby funkcja byłoa identycznościowa.
     **/
    @Unroll
    def 'Beware: #date +/- 1 month gives back the same date'() {
        expect:
            date == PA06DateTime.addAndSubtractOneMonth(date)
        where:
            date << (0..365).collect {
                day -> START_DATE.plusDays(day)
            }
    }

    /**
     * Wróć do slajdów
     */

}
