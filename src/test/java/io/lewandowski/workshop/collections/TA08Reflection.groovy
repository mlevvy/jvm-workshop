package io.lewandowski.workshop.collections

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

import java.time.*
import java.time.format.DateTimeFormatter

import static org.junit.Assert.assertEquals

class TA08Reflection extends WorkshopSpecification {

    def "Reflection in groovy"(){
        given:
            String className = "io.lewandowski.workshop.collections.PA07Reflection"
            String publicMethodName = "publicMethod"
            String privateMethodName = "privateMethod"

        when:
            Object instance = this.getClass().classLoader.loadClass(className)?.newInstance()
            Object resultPublic = instance."$publicMethodName"()
            Object resultPrivate = instance."$privateMethodName"()
        then:
            resultPublic == "PubHello"
            resultPrivate == "PrivHello"
    }

}
