package io.lewandowski.workshop.collections;

import io.lewandowski.workshop.WorkshopSpecification
import spock.lang.Unroll

class TA01EqualsHashCodeCompareTo extends WorkshopSpecification {

    private static PA01Cpu corei3 = new PA01Cpu(2, 4, 4)
    private static PA01Cpu corei5 = new PA01Cpu(4, 4, 6)
    private static PA01Cpu corei7 = new PA01Cpu(4, 8, 8)


    def "Equals symmetry broken example"(){
        given:
            PA01NumberWrapper oneA = new PA01NumberWrapper(1)
            PA01NumberWrapper oneB = new PA01NumberWrapper(1)
        expect: 'this is obvious'
            oneA.equals(oneB)
            oneB.equals(oneA)
        and: 'Cool hidden feature'
            oneB.equals(new Integer(1))
        and: 'Otherwise is broken'
            !new Integer(1).equals(oneB)
    }

    def "Equals transitive broken example"(){
        given:
            PA01NumberWithString theA = new PA01NumberWithString(1, "one")
            PA01Number theB = new PA01Number(1)
            PA01NumberWithString theC = new PA01NumberWithString(1, "red")
        expect: 'symetry is ok: a == b'
            theA.equals(theB) //Symetry is OK. Comparing only numerical field
            theB.equals(theA) //Symetry is OK. Comparing only numerical field
        and: 'symetry is ok: c == b'
            theC.equals(theB) //Symetry is OK. Comparing only numerical field
            theB.equals(theC) //Symetry is OK. Comparing only numerical field
        and: 'transitive is not ok: c != a'
            !theA.equals(theC)
            !theC.equals(theA)

            /** Rady:
             * Porównuj przez obj.getClass() zamiast instanceof - ograniczasz się wtedy ostro do typów jakie porównujesz.
             * Niestety łamie to zasadę Liskov.
             * Używaj kompozycji zamiast dziedziczenia, co działa z zasadą Liskov.
             */

            /** Ćwiczenie:
             * 1. Zamień implementację tak, żeby nie uwzględniała pod typów. HINT: PA01Number, warunek if: obj == null || obj.getClass() != this.getClass()
             * 2. Zamień implementację na całkowicie ślepą. Porównuje tylko "number", ignoruje kolory. Hint: PA01NumberWithString powinien wywołać tylko implementację z nad klasy.
             */

            /**
             * Powrót do slajdów.
             */
    }

    /**
     * Ćwiczenie. Zaimplementuj metodę equals.
     */
    def "Equals: Compare with other cpu model"() {
        given:
            PA01Cpu myi5 = new PA01Cpu(4, 4, 6)
        when:
            boolean equals = myi5.equals(corei5)
            log.info("otheri5: '{}'", myi5)
            log.info("corei5: '{}'", corei5)
        then:
            equals
    }

    /**
     * Ćwiczenie. Zaimplementuj metodę equals.
     */
    def "Equals: Find in list"() {
        given:
            List<PA01Cpu> shop = [corei3, corei5, corei7]
            PA01Cpu myi5 = new PA01Cpu(4, 4, 6)
        when:
            boolean equals = shop.contains(myi5)
        then:
            equals
    }

    /**
     * Ćwiczenie. Zaimplementuj metodę hashCode.
     */
    def "Hashcode: Find in map"() {
        given:
            Map<PA01Cpu, Integer> shop = [(corei3): 1, (corei5): 10, (corei7): 3]
            PA01Cpu myi5 = new PA01Cpu(4, 4, 6)
        when:
            boolean equals = shop.containsKey(myi5)
            log.info("otheri5: '{}'", myi5)
            log.info("corei5: '{}'", corei5)
        then:
            equals
    }

    /**
     * Ćwiczenie. Rozszerz Comparable<PA01Cpu> i zaimplementuj compareTo
     * Hint: Implement using plugin: generateCompareTo and action called: generateCompareTo
     */
    @Unroll
    def "Comparator: Should be sorted"() {
        when:
            TreeSet set = new TreeSet(input)
        then:
            notThrown(ClassCastException)
            set.asList() == [corei3, corei5, corei7]
        where:
            input << [[corei5, corei7, corei3], [corei7, corei3, corei5], [corei3, corei5, corei7]]
    }

}
