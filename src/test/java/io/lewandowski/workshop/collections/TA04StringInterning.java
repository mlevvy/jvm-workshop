package io.lewandowski.workshop.collections;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TA04StringInterning {

    @Test()
    public void stringInternedExplained() {
        //given
        String s1 = new String("ABC");
        String s2 = "ABC";
        String s3 = "ABC";

        //when (Two interned strings are the same)
        assertTrue(s2 == s3);

        //when (Comparing reference to object and string pool)
        assertFalse(s1 == s2);

        //when (Interned strings, will be the same)
        s1 = s1.intern();
        assertTrue(s1 == s2);
    }

    /**
     * Every string is Encoded using UTF-16
     */
    @Test
    public void stringEncoding() throws UnsupportedEncodingException {
        //given
        String input = "Zażółć gęślą jaźń";
        byte[] containsEncoding = input.getBytes(Charset.forName("UTF-8"));

        //when
        String wrongDecode = new String(containsEncoding, "windows-1250");
        String correctDecode = new String(containsEncoding, "UTF-8");
        String autoDecode = new String(containsEncoding);

        //then
        assertFalse(input.equals(wrongDecode));
        assertTrue(input.equals(correctDecode));
        assertTrue(input.equals(autoDecode));

    }

}