package io.lewandowski.workshop.collections;

public class PA01NumberWithString extends PA01Number{

    private final String name;

    public PA01NumberWithString(int number, String name) {
        super(number);
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PA01Number))
            return false;
        if (!(obj instanceof PA01NumberWithString))
            return obj.equals(this);
        PA01NumberWithString that = (PA01NumberWithString) obj;
        return super.equals(obj) && that.name.equals(this.name);
    }

}