package io.lewandowski.workshop.collections;

public class PA07Reflection {

    public String publicMethod(){
        return "PubHello";
    }

    private String privateMethod(){
        return "PrivHello";
    }
}
