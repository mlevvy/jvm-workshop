package io.lewandowski.workshop.collections;

public class PA01NumberWrapper {

    private final Integer number;

    public PA01NumberWrapper(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o instanceof PA01NumberWrapper)
            return number.equals (((PA01NumberWrapper) o).number);

        if (o instanceof Integer)  // One-way interoperability!
            return number.equals(o);

        return false;
    }

}