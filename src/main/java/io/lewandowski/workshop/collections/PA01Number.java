package io.lewandowski.workshop.collections;

public class PA01Number {

    private final Integer number;

    public PA01Number(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PA01Number))
            return false;
        PA01Number that = (PA01Number)obj;

        return that.number.equals(this.number);
    }

}