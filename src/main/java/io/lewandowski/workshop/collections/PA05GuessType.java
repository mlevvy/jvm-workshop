package io.lewandowski.workshop.collections;

import java.util.List;

public class PA05GuessType {

    /**
     * Hint: Mogę pobrać element, będzie on typu Object.
     * Hint: Mogę dodać element, dostanę ostrzeżenie od kompilatora
     */
    static void solveIt1(List s0) {
        Object data = s0.get(0);
        s0.add(1); //Unchecked call
    }

    /**
     * Hint: Mogę pobrać element, będzie on typu Object.
     * Hint: Nie mogę dodać żadnego elementu, ponieważ nie znam implementacji listy.
     */
    static void solveIt2(List<?> s0) {
        Object data = s0.get(0);
        //s0.add(1);
    }

    /**
     * Hint: Mogę pobrać element, znam jego typ. Lista jest producentem.
     * Hint: Nie mogę dodać żadnego elementu, ponieważ nie znam implementacji listy.
     */
    static void solveIt3(List<? extends Number> s0) {
        Number data = s0.get(0);
        //s0.add(1)
    }

    /**
     * Hint: Mogę pobrać element, nie znam jego typu. Lista jest konsumentem.
     * Hint: Nie mogę dodać żadnego elementu, ponieważ nie znam implementacji listy.
     */
    static void solveIt4(List<? super Number> s0) {
        Object data = s0.get(0);
        s0.add(1);
    }

}

