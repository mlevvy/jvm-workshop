package io.lewandowski.workshop.jvm;

public class PL04ReadEnv {

    public static void main(String[] args) throws InterruptedException {
        boolean success = Boolean.parseBoolean(System.getProperty("success"));

        if (!success) {
            System.out.println("Flag not set");
            return;
        }

        assert !assertionsOn();

        System.out.println("This program should FAIL.");
    }

    private static boolean assertionsOn() {
        return PL04ReadEnv.class.desiredAssertionStatus();
    }
}