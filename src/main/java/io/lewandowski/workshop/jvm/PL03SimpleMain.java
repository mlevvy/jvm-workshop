package io.lewandowski.workshop.jvm;

import java.util.concurrent.TimeUnit;

public class PL03SimpleMain {
    public static void main(String[] args) throws InterruptedException {
        TimeUnit.HOURS.sleep(1);
    }
}

/**
 * Instrukcja:
 * * Uruchom tego main'a.
 * ps -ef | grep "PL03SimpleMain" | grep -v grep | awk '{print $2}' |  xargs jstack
 *
 * Grepuje PID ostatniego procesu java (pierwszym jest IDEA), przekazuje go do jstack, który robi zrzut wątków.
 *
 * ćwiczenie: Dodaj "-l" -> pokazuje locki.
 * ćwiczenie: kill -3 PID (drukuje na std-out)
 *
 * Wróć do slajdów tam jest opis.
 **/