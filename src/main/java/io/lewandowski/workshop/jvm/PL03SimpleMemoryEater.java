package io.lewandowski.workshop.jvm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PL03SimpleMemoryEater {

    private static Random r = new Random();

    public static void main(String[] args) throws InterruptedException {

        List<String> neverCleaned = new ArrayList<>();

        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            List<Integer> a = new ArrayList<>();
            for (int j = 0; j < 100_000; j++) {
                a.add(j + r.nextInt(500) + j);
            }
            TimeUnit.MILLISECONDS.sleep(100);
            String someValue = String.valueOf(a.stream().mapToInt(Integer::intValue).sum());
            neverCleaned.add(someValue);
            System.out.println(someValue);
        }

        System.out.println("neverCleaned = " + neverCleaned);

    }
}