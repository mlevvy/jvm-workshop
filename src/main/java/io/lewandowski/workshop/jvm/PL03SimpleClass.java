package io.lewandowski.workshop.jvm;

public class PL03SimpleClass {
    public String sayHello() {
        return new PL03SimpleClass().toString();
    }
}

/**
 * Ćwiczenie 1. Użyj dodatkowych parametrów:
 * -v Verbose
 * -p Show all classes and members
 * -s Print internal type signatures
 * -sysinfo Show system info of classes
 * -constants Show final constants
 *
 * Po wykonaniu wróć do slajdów.
 *
 */

/**
 * Ćwiczenie 2. Użyj dodatkowych parametrów:
 * -v Verbose
 * -p Show all classes and members
 * -s Print internal type signatures
 * -sysinfo Show system info of classes
 * -constants Show final constants
 *
 * Ćwiczenie. Zdefiniuj int a = 5;, ale jej nie drukuj. Sprawdź, że instrukcja zostanie wycięta z bytecodu.
 *
 * Ćwiczenie. Zdefiniuj int a = 5 i ją wydrukuj. Powinieneś zauważyć ją w bytecode.
 *
 * Po wykonaniu wróć do slajdów.
 */