package io.lewandowski.workshop.jvm;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PL02FinalField {
    private int n;

    public PL02FinalField() {
        this.n = 42;
    }

    static PL02FinalField instance;

    public static void initialize(){
        instance = new PL02FinalField();
        /*
         * Stowrzenie obiektu nie jest krokiem atomowym. Składa się z dwóch części:
         * 1. Allocate memory (będzie różny od null)
         * 2. Wywołanie konstruktora (ustalenie zmiennych).
         *
         * Rozwiązanie:
         * Ustalenie pola n jako finalne.
         * Jest to operacja która mówi, że stwrzenie całego konstrutkora ma być atomowe.
         */
    }

    public static int assertSanity() {
        if(instance != null){
            return instance.n;
        }
        return -1;
    }
}
