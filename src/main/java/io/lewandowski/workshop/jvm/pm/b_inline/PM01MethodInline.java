package io.lewandowski.workshop.jvm.pm.b_inline;


public class PM01MethodInline {

    public String businessMethod(String find){
        AppleRepository appleRepository = new MockRepository();
        if(appleRepository == null){
            throw new RuntimeException();
        }
        return appleRepository.get(find);
    }
}

interface AppleRepository{
    public String get(String query);
}

class MockRepository implements AppleRepository {

    @Override
    public String get(String query) {
        return query + query;
    }
}