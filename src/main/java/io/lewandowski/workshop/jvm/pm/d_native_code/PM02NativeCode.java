package io.lewandowski.workshop.jvm.pm.d_native_code;

public class PM02NativeCode {

    public int f(int number) throws Exception {
       int i = 6;
       return number * i;
    }

    public static void main(String[] args) throws Exception {
        for (int i = 1; i <= 10; i++) {
            long a = System.nanoTime();
            int f = new PM02NativeCode().f(484774);
            long b = System.nanoTime();
            System.out.println("Try nr: '" + i + "'. Produced result: '" + f + "', and took: '" + (b - a) + "' nanos");
        }
    }
}