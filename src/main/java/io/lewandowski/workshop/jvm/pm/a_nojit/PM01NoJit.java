package io.lewandowski.workshop.jvm.pm.a_nojit;


public class PM01NoJit {

    private String getValueFrom(AppleRepository appleRepository, String query){
        if(appleRepository == null){
            throw new RuntimeException();
        }
        return appleRepository.get(query);
    }

    public String businessMethod(String find){
        AppleRepository appleRepository = new MockRepository();
        return this.getValueFrom(appleRepository, find);
    }
}

interface AppleRepository{
    public String get(String query);
}

class MockRepository implements AppleRepository{

    @Override
    public String get(String query) {
        return query + query;
    }
}