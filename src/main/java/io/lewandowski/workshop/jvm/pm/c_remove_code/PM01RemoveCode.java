package io.lewandowski.workshop.jvm.pm.c_remove_code;


public class PM01RemoveCode {

    public String businessMethod(String find){
        AppleRepository appleRepository = new MockRepository();
        return appleRepository.get(find);
    }
}

interface AppleRepository{
    public String get(String query);
}

class MockRepository implements AppleRepository {

    @Override
    public String get(String query) {
        return query + query;
    }
}