package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PN01CounterSafe {

    private AtomicInteger count = new AtomicInteger();

    public long getCount() {
        return count.get();
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        count.incrementAndGet();
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        return factors;
    }
}
