package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
class PM01MyFirstCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        log.info("My name is Callable");
        return "Callable";
    }
}
