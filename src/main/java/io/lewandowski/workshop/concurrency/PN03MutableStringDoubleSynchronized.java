package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PN03MutableStringDoubleSynchronized {

    private String myMagicString;

    public String notNullGet() throws InterruptedException {
        while (getMyMagicString() == null) ;
        log.info("Got my value");
        return myMagicString;
    }

    public synchronized void setMyMagicString(String myMagicString) throws InterruptedException {
        log.info("Before set");
        this.myMagicString = myMagicString;
        log.info("After set");
    }

    public synchronized String getMyMagicString() {
        return myMagicString;
    }
}
