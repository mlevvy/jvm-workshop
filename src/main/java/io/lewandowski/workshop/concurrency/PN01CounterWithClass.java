package io.lewandowski.workshop.concurrency;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class PN01CounterWithClass {

    private AtomicReference<Counter> count = new AtomicReference<>(new Counter(0));

    public long getCount() {
        return count.get().getCount();
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        count.updateAndGet(counter -> new Counter(counter.getCount() + 1));
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        return factors;
    }
}

final class Counter {

    private final Integer count;

    public Counter(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }
}
