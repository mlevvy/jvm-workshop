package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
public class PN07CountDownLatch{
    private CountDownLatch latch;

    public PN07CountDownLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    public void buildProject() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(100);
        log.info("Build projects started");
        this.latch.countDown();
        log.info("Latch counted down");

    }

    public String deployProject() throws InterruptedException {
        log.info("Awaiting for builds to complete...");
        latch.await();
        log.info("Awaiting completed...");
        return "Deployed";
    }

}
