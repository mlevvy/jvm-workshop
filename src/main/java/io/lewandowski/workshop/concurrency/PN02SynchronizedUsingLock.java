package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PN02SynchronizedUsingLock {

    private long count = 0;

    private Lock lock = new ReentrantLock();

    public long getCount() {
        lock.lock();
        try {
            return count;
        } finally {
            lock.unlock();
        }
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        lock.lock();
        try {
            ++count;
        } finally {
            lock.unlock();
        }
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        TimeUnit.MILLISECONDS.sleep(1);
        return factors;
    }
}
