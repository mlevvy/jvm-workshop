package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;

public class PN01NotSafe {

    private long count = 0;

    public long getCount() {
        return count;
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        ++count;
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        return factors;
    }
}
