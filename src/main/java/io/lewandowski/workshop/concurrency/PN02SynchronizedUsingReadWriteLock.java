package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PN02SynchronizedUsingReadWriteLock {

    private long count = 0;

    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public long getCount() {
        lock.readLock().lock();
        try {
            return count;
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        lock.writeLock().lock();
        try {
            ++count;
        } finally {
            lock.writeLock().unlock();
        }
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        TimeUnit.MILLISECONDS.sleep(1);
        return factors;
    }
}
