package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PN02SynchronizedFaster {

    private long count = 0;

    public synchronized long getCount() {
        return count;
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        synchronized (this) {
            ++count;
        }
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        TimeUnit.MILLISECONDS.sleep(1);
        return factors;
    }
}
