package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PO01CanNotInterrupt extends Thread {

    @Override
    public void run() {
        while (true) {
            log.info("Working...");
        }
//        log.info("Done");
    }

}
