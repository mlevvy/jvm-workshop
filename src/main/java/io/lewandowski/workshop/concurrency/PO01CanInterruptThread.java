package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PO01CanInterruptThread extends Thread {

    @Override
    public void run() {
        while (true) {
            log.info("Working...");
            if (Thread.currentThread().isInterrupted()) {
                log.info("Oh my good. I got an interruption!");
                break;
            }
        }
        log.info("Done");
    }

}
