package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class PN02SyncReEntrace {

    public synchronized String outer() throws InterruptedException {
        log.info("Into outer");
        return inner();
    }

    public synchronized String inner() throws InterruptedException {
        log.info("Into inner - before sleep");
        TimeUnit.SECONDS.sleep(2);
        log.info("Into inner - after sleep");
        return "OK";
    }
}
