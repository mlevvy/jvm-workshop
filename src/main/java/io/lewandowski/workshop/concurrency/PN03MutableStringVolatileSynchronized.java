package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PN03MutableStringVolatileSynchronized {

    private volatile String myMagicString;

    public String notNullGet() throws InterruptedException {
        while (getMyMagicString() == null) ;
        log.info("Got my value");
        return myMagicString;
    }

    public void setMyMagicString(String myMagicString) throws InterruptedException {
        log.info("Before set");
        this.myMagicString = myMagicString;
        log.info("After set");
    }

    public String getMyMagicString() {
        return myMagicString;
    }
}
