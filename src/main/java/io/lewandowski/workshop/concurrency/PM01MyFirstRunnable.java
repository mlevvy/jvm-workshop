package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PM01MyFirstRunnable implements Runnable {

    @Override
    public void run() {
        log.info("My name is Runnable");
    }
}
