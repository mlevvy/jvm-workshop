package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
public class P06Collections {

    private final List<Integer> theList = new ArrayList<>();

    private final Random randomGenerator = new Random();

    public void addRandom() {
        int i = randomGenerator.nextInt(10_000);
        log.info("Adding {}", i);
        theList.add(i);
    }

    public void showMeException() {
        for (Integer i : theList) {
            log.info("Element is: {}", i);
        }
    }


}
