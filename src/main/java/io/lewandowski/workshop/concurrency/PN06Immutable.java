package io.lewandowski.workshop.concurrency;

import java.util.ArrayList;
import java.util.List;

/**
 * Czasami, żeby dzielić obiekt pomiędzy wątkami, możemy użyć do tego mechanizmów synchronizacji które poznaliśmy wcześniej.
 * Możemy stworzyć też obiekty które są niezmienne i je publikować.
 * <p>
 * Obiekt jest niezmienny jeżeli:
 * • Stan nie jest modyfikowany po zakończeniu pracy konstruktora (może składać się z mutowalnych pól)
 * • Wszystkie pola są finalne
 * • Referencja do samego siebie nie usieka (to znaczy nie jest przypisane do żadnej zminnej)
 */
public class PN06Immutable {

    private final List<String> stooges = new ArrayList<>();

    public PN06Immutable() {
        stooges.add("Fabiański");
        stooges.add("Skorupiński");
        stooges.add("Szczęsny");
        stooges.add("Tytoń");
    }

    public boolean isGoalKeeper(String name) {
        return stooges.contains(name);
    }
}
