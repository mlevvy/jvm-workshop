package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j
public class PN00SimpleSafe {

    public ArrayList<Integer> business(int number) {
        log.info("Start");
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        log.info("Stop");
        return factors;
    }
}
