package io.lewandowski.workshop.architecture.solid.isp.bad;

public class PY04ISPBad {
    void sendFile(FileServer server, String file){
        server.send(file);
    }

    String getFile(FileServer server, String path){
        return server.get(path);
    }
}

/** Ten interfejs jest problemem. Powinien być podzielony na dwa*/
interface FileServer {
    void send(String string);
    String get(String path);
}

class RWFileServer implements FileServer{

    @Override
    public void send(String string) {
        //Something works
    }

    @Override
    public String get(String path) {
        return "Something";
    }
}

/**Ponieważ pewnego dnia, ktoś wpadł na pomysł, że będzie baza do zapisu i do odczytu..*/
class ROFileServer implements FileServer{

    @Override
    public void send(String string) {
        throw new RuntimeException("Not supported");
    }

    @Override
    public String get(String path) {
        return "Something";
    }
}


