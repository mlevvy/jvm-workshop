package io.lewandowski.workshop.architecture.solid.srp.good;

class PY01PollutionAnalyzer {

    private PY01Meter meter;

    public PY01PollutionAnalyzer(PY01Meter meter) {
        this.meter = meter;
    }

    public boolean isDangerLevel() {
        return meter.getCurrentPm10() >= 150;
    }

}
