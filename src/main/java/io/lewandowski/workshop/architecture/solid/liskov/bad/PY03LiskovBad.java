package io.lewandowski.workshop.architecture.solid.liskov.bad;

public class PY03LiskovBad {
    public int calculateSize(Text text) {
        return text.getLength();
    }
}

class Text {
    String text;

    Integer getLength() {
        return text.length();
    }
}

class QuotedText extends Text {
    Integer getLength() {
        return text.length() + 2;
    }
}

