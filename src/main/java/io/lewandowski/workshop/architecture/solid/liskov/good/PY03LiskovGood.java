package io.lewandowski.workshop.architecture.solid.liskov.good;

public class PY03LiskovGood {
    public int calculateSize(Measurable text) {
        return text.getLength();
    }
}

/**
 * Zastosowano:
 * * Używaj interfejsu do przedstawienia zachowania
 * * Komponuj obiekty, zamiast dziedziczyć
 */

interface Measurable {
    Integer getLength();
}

class Text implements Measurable {

    public Text(String text) {
        this.text = text;
    }

    String text;

    @Override
    public Integer getLength() {
        return text.length();
    }
}

class QuotedText implements Measurable {

    private Text text;

    public QuotedText(Text text) {
        this.text = new Text("'" + text.text + "'");
    }

    @Override
    public Integer getLength() {
        return text.getLength();
    }
}

