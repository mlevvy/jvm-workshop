package io.lewandowski.workshop.architecture.solid.srp.bad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PY01PollutionMeter {

    private static final Logger log = LoggerFactory.getLogger(PY01PollutionMeter.class);

    private Integer pm10micrograms;

    public Integer updatePm10() {
        //call web-service
        //parse value
        pm10micrograms = 165;
        return pm10micrograms;
    }

    public boolean isDangerLevel() {
        return pm10micrograms >= 150;
    }

    public void printResults() {
        log.info("PM10 level is {}", pm10micrograms);
    }


}
