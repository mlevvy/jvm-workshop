package io.lewandowski.workshop.architecture.solid.isp.good;

public class PY04ISPGood {
    void sendFile(WritingFileServerServer server, String file){
        server.send(file);
    }

    void getFile(ReadingFileServerServer server, String path){
        server.get(path);
    }

    void getAndSend(ReadingWritingFileServer server, String path){
        server.get(path);
        server.send("Oh");
    }

    public static void main(String[] args) {
//        new PY04ISPGood().sendFile(new ROFileServer(), ""); //API wymaga tylko serwera do zapisu i odczytu, nie mogę użyć serwera RO
        new PY04ISPGood().getFile(new RWFileServer(), ""); //API wymaga tylko serwera do zapisu, mogę użyć serwera RW
    }
}

interface ReadingFileServerServer {
    String get(String path);
}

interface WritingFileServerServer {
    void send(String string);
}

interface ReadingWritingFileServer extends ReadingFileServerServer, WritingFileServerServer {
}

class RWFileServer implements ReadingWritingFileServer{

    @Override
    public void send(String string) {
        //Something works
    }

    @Override
    public String get(String path) {
        return "Something";
    }
}

class ROFileServer implements ReadingFileServerServer {

    @Override
    public String get(String path) {
        return "Something";
    }
}


