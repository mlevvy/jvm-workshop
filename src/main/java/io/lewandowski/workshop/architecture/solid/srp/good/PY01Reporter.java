package io.lewandowski.workshop.architecture.solid.srp.good;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PY01Reporter {

    private static final Logger log = LoggerFactory.getLogger(io.lewandowski.workshop.architecture.solid.srp.good.PY01Reporter.class);


    private PY01Meter meter;

    public PY01Reporter(PY01Meter meter) {
        this.meter = meter;
    }

    public void printResults() {
        log.info("PM10 level is {}", meter.getCurrentPm10());
    }

}
