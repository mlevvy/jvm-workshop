package io.lewandowski.workshop.architecture;


import com.javamex.classmexer.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PX01EmptyClass {

    private static final Logger log = LoggerFactory.getLogger(PX01EmptyClass.class);

    /** ćwiczenie:
     * 0. Dodaj classmexer.jar na class path. (PPM na plik)
     * 0. Dodaj VM argument: -javaagent:/YOUR_PATH/jars/classmexer.jar
     * 1. Dodaj zmienną typu Integer. Sprawdź czy rozmiar klasy jest większy.
     * 2. Zmień typ na Listę integerów (Arrays.asList(1,2,3). Sprawdź rozmiar
     * 2. Zmień typ na tablicę integerów (new int[]{1,i,3};). Sprawdź rozmiar
     *
     * Dodatkowo:
     * Zobacz objectsizefetcher - przykładowa implementacja
     */

    public static void main(String[] args) {
        PX01EmptyClass v = new PX01EmptyClass();
        long objectSize = MemoryUtil.deepMemoryUsageOf(v);
        log.info("Object '{}' size is: '{}'", v.toString(), objectSize);
    }
}
