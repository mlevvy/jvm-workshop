package io.lewandowski.workshop.architecture.eventsourcing.service;

import io.lewandowski.workshop.architecture.eventsourcing.domain.AggregateId;
import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import io.lewandowski.workshop.architecture.eventsourcing.repository.EventSourceRepository;

public class CommandService {

    private EventSourceRepository eventSourceRepository;

    public CommandService(EventSourceRepository eventSourceRepository) {
        this.eventSourceRepository = eventSourceRepository;
    }

    public void pressKey(AggregateId aggregateId, char theChar){
        eventSourceRepository.addEvent(new KeyPressed(aggregateId, theChar));
    }
}
