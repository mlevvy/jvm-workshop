package io.lewandowski.workshop.architecture.eventsourcing.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KeyPressed {
    private AggregateId aggregateId;
    private char value;
}
