package io.lewandowski.workshop.architecture.eventsourcing.models;


import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class UniqueKeys implements ReadModel{
    private Set<Character> keys = new HashSet<>();

    public Integer getCount(){
        return keys.size();
    }

    public void update(KeyPressed event) {
        keys.add(event.getValue());
    }

    @Override
    public ReadModel construct() {
        return new UniqueKeys();
    }
}
