package io.lewandowski.workshop.architecture.eventsourcing.repository;

import io.lewandowski.workshop.architecture.eventsourcing.models.SumNumbers;
import io.lewandowski.workshop.architecture.eventsourcing.models.UniqueKeys;
import lombok.Data;

@Data
public class ReadModelsRepository {

    ModelsRepository<SumNumbers> sumModels = new ModelsRepository<>(SumNumbers::new);

    ModelsRepository<UniqueKeys> uniqueNumbers = new ModelsRepository<>(UniqueKeys::new);

}
