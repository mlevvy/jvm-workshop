package io.lewandowski.workshop.architecture.eventsourcing.models;

import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;

public interface ReadModel {
    void update(KeyPressed event);
    ReadModel construct();
}
