package io.lewandowski.workshop.architecture.eventsourcing.repository;

import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;

import java.util.ArrayList;
import java.util.List;

public class EventSourceRepository {
    private List<KeyPressed> events = new ArrayList<>();

    public void addEvent(KeyPressed event){
        events.add(event);
    }

    public Integer getPosition(){
        return events.size();
    }

    public List<KeyPressed> getEventsSince(Integer currentPosition) {
        return events.subList(currentPosition, events.size());
    }
}
