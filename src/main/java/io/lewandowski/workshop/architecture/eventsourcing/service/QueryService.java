package io.lewandowski.workshop.architecture.eventsourcing.service;

import io.lewandowski.workshop.architecture.eventsourcing.domain.AggregateId;
import io.lewandowski.workshop.architecture.eventsourcing.models.SumNumbers;
import io.lewandowski.workshop.architecture.eventsourcing.models.UniqueKeys;
import io.lewandowski.workshop.architecture.eventsourcing.repository.EventSourceRepository;
import io.lewandowski.workshop.architecture.eventsourcing.repository.ModelsRepository;
import io.lewandowski.workshop.architecture.eventsourcing.repository.ReadModelsRepository;

public class QueryService {

    private EventSourceRepository eventSourceRepository;

    private ReadModelsRepository readModelsRepository;

    public QueryService(EventSourceRepository eventSourceRepository, ReadModelsRepository readModelsRepository) {
        this.eventSourceRepository = eventSourceRepository;
        this.readModelsRepository = readModelsRepository;
    }

    public int getSum(AggregateId aggregateId){
        ModelsRepository<SumNumbers> sumModel = readModelsRepository.getSumModels();
        sumModel.update(eventSourceRepository);
        return sumModel.get(aggregateId).getSumNumbers();
    }

    public int getUniqueNumbers(AggregateId aggregateId){
        ModelsRepository<UniqueKeys> sumModel = readModelsRepository.getUniqueNumbers();
        sumModel.update(eventSourceRepository);
        return sumModel.get(aggregateId).getCount();
    }
}
