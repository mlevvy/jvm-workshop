package io.lewandowski.workshop.architecture.eventsourcing;

import io.lewandowski.workshop.architecture.eventsourcing.domain.AggregateId;
import io.lewandowski.workshop.architecture.eventsourcing.service.CommandService;
import io.lewandowski.workshop.architecture.eventsourcing.service.QueryService;

public class KeyTracker {

    private final CommandService commandService;

    private final QueryService queryService;

    public KeyTracker(CommandService commandService, QueryService queryService) {
        this.commandService = commandService;
        this.queryService = queryService;
    }

    public void pressKey(String keyboard, char theChar){
        commandService.pressKey(new AggregateId(keyboard), theChar);
    }

    public int getNumbersSum(String keyboard){
        return queryService.getSum(new AggregateId(keyboard));
    }

    public int getUniqueChars(String keyboard){
        return queryService.getUniqueNumbers(new AggregateId(keyboard));
    }


}
