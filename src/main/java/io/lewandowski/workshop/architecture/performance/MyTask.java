package io.lewandowski.workshop.architecture.performance;

import java.util.ArrayList;

public class MyTask {

    public ArrayList<Integer> business(int number) {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        return factors;
    }

}
