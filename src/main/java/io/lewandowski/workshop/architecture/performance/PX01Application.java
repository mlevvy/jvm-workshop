package io.lewandowski.workshop.architecture.performance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@RestController
public class PX01Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PX01Application.class, args);
    }

    @GetMapping("real/{number}")
    public MyResponse realGet(@PathVariable("number") int number){
        return new MyResponse(new MyTask().business(number));
    }

    @GetMapping("fake/{number}")
    public MyResponse fakeGet(@PathVariable("number") int number){
        return new MyResponse(Arrays.asList(1,17,289,29269,497573,8458741));
    }

    @GetMapping("sleep/{number}")
    public MyResponse constantTimeGet(@PathVariable("number") int number) throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return new MyResponse(Arrays.asList(1,17,289,29269,497573,8458741));
    }

}

class MyResponse {
    private final List<Integer> result;

    MyResponse(List<Integer> result) {
        this.result = result;
    }

    public List<Integer> getResult() {
        return result;
    }
}