package io.lewandowski.workshop.architecture;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Slf4j
public class PW01CompletableFuture {

    private Random random = new Random();

    public String find(String query) throws InterruptedException {
        int duration = random.nextInt(1000);
        log.info("Processing query '{}' will take '{}' milliseconds", query, duration);
        TimeUnit.MILLISECONDS.sleep(duration);
        log.info("Response received for query {}", query);
        return new StringBuilder(query).reverse().toString();
    }
}
