package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PN03MutableStringDoubleSynchronized {

    private String myMagicString;

    public String notNullGet() throws InterruptedException {
        while (getMyMagicString() == null) ;
        return myMagicString;
    }

    public synchronized void setMyMagicString(String myMagicString) throws InterruptedException {
        this.myMagicString = myMagicString;
    }

    public String getMyMagicString() {
        return myMagicString;
    }
}
