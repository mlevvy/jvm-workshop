package io.lewandowski.workshop.architecture;


import io.lewandowski.workshop.architecture.performance.MyTask;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PX02Benchmark {

    private static final Logger log = LoggerFactory.getLogger(PX02Benchmark.class);

    @Benchmark()
    public void helloWorld() {
        Double magicRandom = Math.PI * 100_000;
        log.info("Hello {}", magicRandom);
        new MyTask().business(magicRandom.intValue());
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(PX02Benchmark.class.getSimpleName()).forks(1).warmupIterations(1).build();

        new Runner(options).run();
    }
}
