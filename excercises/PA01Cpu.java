package io.lewandowski.workshop.collections;

public class PA01Cpu {

    private final int cores;
    private final int threads;
    private final int cache;

    public PA01Cpu(int cores, int threads, int cache) {
        this.cores = cores;
        this.threads = threads;
        this.cache = cache;
    }

    @Override
    public String toString() {
        return "Cpu{" +
                "cores=" + cores +
                ", threads=" + threads +
                ", cache=" + cache +
                '}';
    }
}