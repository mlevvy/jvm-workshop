package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class PN02DualSync {

    public synchronized String businessA() throws InterruptedException {
        log.info("Business A started");
        TimeUnit.SECONDS.sleep(1);
        log.info("Business A will end soon");
        return "A";
    }

    public synchronized String businessB() throws InterruptedException {
        log.info("Business B started");
        TimeUnit.SECONDS.sleep(1);
        log.info("Business B will end soon");
        return "B";
    }
}
