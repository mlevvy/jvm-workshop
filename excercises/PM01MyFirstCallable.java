package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
class PM01MyFirstCallable implements Callable {

    @Override
    public Object call() throws Exception {
        return null;
    }
}
