package io.lewandowski.workshop.architecture.eventsourcing.repository;

import io.lewandowski.workshop.architecture.eventsourcing.domain.AggregateId;
import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import io.lewandowski.workshop.architecture.eventsourcing.models.ReadModel;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Data
public class ModelsRepository<T extends ReadModel> {

    private Integer currentPosition = 0;

    private Map<AggregateId, T> uniqueKeysMap = new HashMap<>();

    private Supplier<T> supplier;

    public ModelsRepository(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public T get(AggregateId aggregateId) {
        return uniqueKeysMap.computeIfAbsent(aggregateId, it -> supplier.get());
    }

    /** Hint:
     * Ta metoda powinna sprawdzić czy mój model jest aktualny (to znaczy czy zawiera ostatni event. Jeżeli nie powinna wyciągnąć brakujące zdarzenia i je za aplikować
     */
    public void update(EventSourceRepository eventSourceRepository) {
        //TODO Implement me 2

    }

    /** Hint:
     * Ta metoda powinna zaaplikować wszystkie eventy. Każdy dla swojej klawiatury (AggregateId)
     * Po zakończeniu pracy, powinna przesunąć wskaźnik swojej aktualności.
     */
    private void update(List<KeyPressed> events) {
        //TODO Implement me 3
    }
}
