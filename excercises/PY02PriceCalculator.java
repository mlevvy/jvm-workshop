package io.lewandowski.workshop.architecture.solid.ocp;

/**
 * ćwiczenie:
 * zaimplementuj metodę calculateFee tak, żeby była niezależna na dodanie nowej klasy. HINT: dodaj abstrakcyjną metodę.
 */

public class PY02PriceCalculator {

    public double calculateFee(Offer offer) {
        if (offer instanceof NormalBuy) {
            return offer.price * 0.1;
        }
        if (offer instanceof InstallmentPay) {
            return offer.price * 0.11;
        }
        return 0;
    }
}

abstract class Offer {
    int price;

    abstract String formatPrice();
}

class NormalBuy extends Offer {


    @Override
    String formatPrice() {
        return "Cena: " + price;
    }
}

class InstallmentPay extends Offer {

    @Override
    String formatPrice() {
        return "Cena: " + price / 10 + " * 10 rat";
    }
}
