package io.lewandowski.workshop.concurrency;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.ThreadFactory;

public class PP01CustomizedScheduler {

    /**
     * ThreadFactory jest klasą z biblioteki standardowej
     * ThreadFactoryBuilder jest klasów z Guavy
     */

    public static ThreadFactory getThreadFactory(){
        return  new ThreadFactoryBuilder()
                .setNameFormat("CustomName")
                .build();
    }

    public static ThreadFactory getThreadFactoryWithThreadNumber(){
        return  new ThreadFactoryBuilder()
                .setNameFormat("CustomName")
                .build();
    }
}
