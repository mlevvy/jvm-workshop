package io.lewandowski.workshop.architecture.eventsourcing.models;

import io.lewandowski.workshop.architecture.eventsourcing.domain.KeyPressed;
import lombok.Data;

@Data
public class SumNumbers implements ReadModel{
    private Integer sumNumbers = 0;

    /** Hint:
     * Dostajemy zdarzenie wcisnięcia klawisza. Wiemy że ten konkretny model powinien sumować tylko cyfry. Powinien to robić.
     */
    public void update(KeyPressed event) {
        //TODO Implement me
    }

    @Override
    public ReadModel construct() {
        return new SumNumbers();
    }
}
