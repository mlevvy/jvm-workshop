package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PN04LocalStorage {

    private ThreadLocal<String> myThreadLocal = new ThreadLocal<>();

    public void setMyValue(String newValue) {
    }

    public String getMyThreadLocal() {
        String threadValue = myThreadLocal.get();
        log.info("Returning value: {}", threadValue);
        return threadValue;
    }
}
