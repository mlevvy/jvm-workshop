package io.lewandowski.workshop.concurrency;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Slf4j
public class P06CollectionsWithSynchronizedCollectionsAndLocking {

    private final List<Integer> theList = Collections.synchronizedList(new ArrayList<>());

    private final Random randomGenerator = new Random();

    public void addRandom() {
        int i = randomGenerator.nextInt(10_000);
        log.info("Adding {}", i);
        theList.add(i);
    }

    public synchronized void showMeException() {
        for (Integer i : theList) {
            log.info("Element is: {}", i);
        }
    }


}
