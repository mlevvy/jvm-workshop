package io.lewandowski.workshop.concurrency;


import java.util.ArrayList;
import java.util.List;

public class PN01CounterWithClass {

    private Counter count = new Counter(0);

    public long getCount() {
        return count.getCount();
    }

    public List<Integer> doBusiness(int number) throws InterruptedException {
        count = new Counter(count.getCount() + 1);
        return factor(number);
    }

    private List<Integer> factor(int number) throws InterruptedException {
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
            }
        }
        return factors;
    }
}

final class Counter {

    private final Integer count;

    public Counter(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }
}
