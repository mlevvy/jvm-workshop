#!/bin/bash
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 argument required, $# provided"
[ -d $1 ] || die "solutions or excercises argument required, $1 provided"

for f in `ls $1`
do
  echo "Changing $f file..."
  find src -name $f -exec cp $1/$f {} \;
done
